'''
App initialization

'''
# imports
from flask import Flask, render_template, url_for, flash, redirect, session, request
from flask_bootstrap import Bootstrap
from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager, UserMixin, current_user, logout_user
from flask_script import Manager
from flask_migrate import Migrate

# initialize app
app = Flask(__name__)
app.config.from_object('config')
Bootstrap(app)
db = SQLAlchemy(app)
lm = LoginManager(app)
migrate = Migrate(app, db)



# import forms and views
from app import forms, models, oauth, scheduler, views
from scheduler import publisher
