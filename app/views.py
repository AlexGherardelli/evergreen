'''
Define routes for Evergreen social app

'''
# import app package
from flask import Flask, render_template, url_for, flash, redirect, session, request
from app import app, lm, db
from forms import Tweet
from models import User, Post
from oauth import OAuthSignIn, TwitterSignIn
from flask_login import current_user, logout_user, login_user, login_required
import time
from datetime import datetime, timedelta

# loads a user by its primary key
@lm.user_loader
def load_user(id):
    return User.query.get(int(id))

@app.route("/")
def index():
    if current_user.is_authenticated:
        return redirect(url_for("scheduler"))
    else:
        return render_template("index.html")

@app.route('/authorize/<provider>')
def oauth_authorize(provider):
    if not current_user.is_anonymous:
        return redirect(url_for('index'))
    oauth = OAuthSignIn.get_provider(provider)
    return oauth.authorize()


@app.route('/callback/<provider>')
def oauth_callback(provider):
    if not current_user.is_anonymous:
        return redirect(url_for('index'))
    oauth = OAuthSignIn.get_provider(provider)
    social_id, username, email, access_token, access_token_secret = oauth.callback()
    if social_id is None:
        flash('Authentication failed.')
        return redirect(url_for('index'))
    user = User.query.filter_by(social_id=social_id).first()
    if not user:
        user = User(social_id=social_id, nickname=username, email=email, access_token=access_token, access_token_secret=access_token_secret)
        db.session.add(user)
        db.session.commit()
    login_user(user, True)
    return redirect(url_for('index'))

@app.route("/scheduler", methods=['GET', 'POST'])
@login_required
def scheduler():
    form = Tweet()
    if request.method == "POST":
        if form.validate_on_submit():
            day = form.days.data
            # next_date = datetime.utcnow() + timedelta(days = day)
            next_date = datetime.utcnow() + timedelta(minutes = day) # TESTING: remove when working
            post = Post(body=form.tweet.data, timestamp=datetime.utcnow(), days=day, scheduled=next_date, user=current_user)
            db.session.add(post)
            db.session.commit()
            flash("Tweet sent (hopefully!)")
            return redirect(url_for("viewer"))
        else:
            flash("Input invalid. Please try again")
            print(form.errors)
    return render_template("scheduler.html", form=form)

@app.route("/viewer")
@login_required
def viewer():
    # get posts from database
    posts = Post.query.filter_by(user = current_user)
    return render_template("viewer.html", posts = posts)

@app.route('/delete_tweet/<int:post_id>')
def delete_tweet(post_id):
    deleted_post = Post.query.get(post_id)
    db.session.delete(deleted_post)
    db.session.commit()
    return redirect(url_for('viewer'))


@app.route("/about")
@login_required
def about():
    return render_template("about.html")


@app.route('/logout')
@login_required
def logout():
    logout_user()
    return redirect(url_for('index'))

# 404 Not Found
@app.errorhandler(404)
def page_not_found(e):
    return render_template('404.html'), 404

# 403 Forbidden
@app.errorhandler(403)
def forbidden(e):
    return render_template('403.html'), 403

# 410 Gone
@app.errorhandler(410)
def gone(e):
    return render_template('410.html'), 410

# 500 Internal Server Error
@app.errorhandler(500)
def internal_server_error(e):
    return render_template('404.html'), 500

