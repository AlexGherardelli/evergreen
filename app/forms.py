'''
class wtforms.fields.DateField(default field arguments, format='%Y-%m-%d')
    Same as DateTimeField, except stores a datetime.date.
class wtforms.fields.DateTimeField(default field arguments, format='%Y-%m-%d %H:%M:%S')
    A text field which stores a datetime.datetime matching a format.

class wtforms.fields.SelectField(default field arguments, choices=[], coerce=unicode, option_widget=None)
Select fields keep a choices property which is a sequence of (value, label) pairs. The value portion can be any type in theory, but as form data is sent by the browser as strings, you will need to provide a function which can coerce the string representation back to a comparable object.

'''
from flask_wtf import FlaskForm
from wtforms import SubmitField, TextAreaField, TextField, DateTimeField, DateField, IntegerField, validators

class Tweet(FlaskForm):
    tweet = TextAreaField(u'Tweet', [validators.required(), validators.length(max=140)], render_kw={"placeholder": "Tweet something", "maxlength": "140"})
    days = IntegerField(u'Days', [validators.required(), validators.NumberRange(min=0, max=366)])
    submit = SubmitField('Submit')