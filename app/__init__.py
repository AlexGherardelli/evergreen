'''
App initialization

'''
# imports
from flask import Flask
from flask_bootstrap import Bootstrap
from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager, UserMixin
from flask_script import Manager
from flask_migrate import Migrate

# initialize app
app = Flask(__name__)
app.config.from_object('config')
Bootstrap(app)
db = SQLAlchemy(app)
lm = LoginManager(app)
migrate = Migrate(app, db)



from app import admin, forms, models, oauth, views
