
# Login pseudocode

```html 
{% if user is logged in %}
    {% extends "base.html" %}
            <div class="row center center-horizontally">
            <div class="col-md-12">
                    <h1> Hello {{session.name}} </h1>
            </div>
        </div>
{% else %}
    {% extends "error.html" %}
    {% block content %}
            <div class="row center center-horizontally">
                <div class="col-md-12">
                        <a href="{{url_for("login")}}">
                            <button class="btn btn-default btn-primary">Sign in with Twitter</button>
                        </a>
                </div>
            </div>
    {% endblock %}
{% endif %}
```