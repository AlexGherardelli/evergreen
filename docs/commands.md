# automatic compiles scss files in directory
sass --watch --scss app/static/css/style.scss:app/static/css/style.css

# get required packages
pip freeze > requirements.txt


# Adding a post from python CLI
u = models.User.query.get(1)
p = models.Post(body='TEST: a first tweet from Evergreen', timestamp=datetime.datetime.utcnow(), user=u)
db.session.add(p)
db.session.commit()

posts = models.Post.query.filter_by(user = u).all()

# Migration scripts

from flask_migrate import Migrate
migrate = Migrate(app, db)

$ flask db init # initialize
$ flask db migrate # first migration
$ flask db upgrade # upgrade migration
$ flask db --help # help
https://flask-migrate.readthedocs.io/en/latest/


# Date and time

class datetime.timedelta([days[, seconds[, microseconds[, milliseconds[, minutes[, hours[, weeks]]]]]]])

import time
from datetime import datetime, date, time, timedelta

now = datetime.utcnow()
days = 3
delta = timedelta(days=days)
next_date = now + delta



p = models.Post(body='TEST: a first tweet from Evergreen', timestamp=now, days=days, scheduled=next_date, user=u)
p2 = models.Post(body='TEST: another test', timestamp=datetime.utcnow(), days=5, scheduled=date2, user=u)
db.session.add(p)
db.session.commit()

posts = models.Post.query.filter_by(user = u).all()



# Scheduler.py
from app import models, db
import time
from datetime import datetime, timedelta

## Delete all posts
p = models.Post.query.all()
for i in p:
    db.session.delete(i)
    db.session.commit()


for post in p:
    current = post.scheduled
    print "current: ", current.strftime("%a, %d %b %Y %H:%M:%S")
    next = post.scheduled + timedelta(post.days)
    print "next: ", next.strftime("%a, %d %b %Y %H:%M:%S")
    m = models.Post(scheduled=next)
    print "Post date", m.d



m = models.Post(scheduled=next_date)
print "Post date", m







# Twitter testing account
EvergreenTestin
j1306716@mvrht.com
evergreen1987