# Evergreen Social Management

Evergreen is a social management app, designed to easily repost content on Twitter.

## Features
* Twitter login
* Repost your content at a random interval
* View your scheduled tweets


## Getting Started

To use Evergreen, clone or download the repository and install the dependencies with: ```pip install -r requirements.txt ```.

Run Evergreen in development server by executing 
```python run.py``` 

Run Evergreen on your server by:
*TODO

## Wishlist and roadmap

* Add images to tweets
* Add auto link shortening with Bit.ly integration
* Add Facebook integration
* Add post on a specific date (e.g. every 25th December)
* Add choosing an interval (e.g. schedule post every 2 to 7 days)
* Add ending date for repost (e.g. schedule [everyday] until 31st December)

## License

GNU GPLv3
