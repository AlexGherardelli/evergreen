'''
Handles configuration for application

THIS FILE SHOULD BE OUT OF VERSION CONTROL AND NEVER TO BE PUBLISHED ONLINE
'''

import os
from app import app
basedir = os.path.abspath(os.path.dirname(__file__))

# Configuration for forms
WTF_CSRF_ENABLED = True

# Application secret key
SECRET_KEY = 'you-will-never-guess'

# Database configuration
SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(basedir, 'app.db')
SQLALCHEMY_MIGRATE_REPO = os.path.join(basedir, 'db_repository')
SQLALCHEMY_TRACK_MODIFICATIONS = True

# Social configuration
app.config['OAUTH_CREDENTIALS'] = {
    # 'facebook': {
    #     'id': 'API id',
    #     'secret': 'API secret'
    # },
    'twitter': {
        'id': 'KSVhI8hs2N5l4p0tMmNY2z4gE',
        'secret': 'DT6qteAXkstTMSEexAbuuIB7DWrySYxOaNhwsRF45zDs6KxGoW'
    }
}