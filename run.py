#! venv/bin/python
from app import *
import os
from app.models import User, Post
import time
from datetime import datetime, timedelta

#Run in debug mode, development server
if __name__ == '__main__':
    db.create_all()

    # Run in Cloud9 IDE Development Server
    app.run(host=os.getenv('IP', '0.0.0.0'), port=int(os.getenv('PORT', 8080)), debug=True)


# Run app in production mode
# if __name__ = '__main__':
#     app.run()