#! venv/bin/python
# import forms and views
from auth import *
from admin import admin
from api import api
from models import *
from views import *
import os

#Run in debug mode, development server
if __name__ == '__main__':
    publisher()
    db.create_all()
    # Run in Cloud9 IDE Development Server
    app.run(host=os.getenv('IP', '0.0.0.0'), port=int(os.getenv('PORT', 8080)), debug=True)

# Run app in production mode
# if __name__ = '__main__':
#     app.run()