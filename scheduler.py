#! venv/bin/python
'''
SCHEDULER
This script queries the database for all posts due in 10 minutes. This script does not run as main Evergreen app,
but scheduled as crontab job

'''
from app import *
from app.models import User, Post
from app.views import *
import time
from datetime import datetime, timedelta
from flask_login import current_user, logout_user, login_user, login_required



def publisher():
    now = datetime.utcnow()
    future = now + timedelta(minutes = 50)

    posts = Post.query.all()

    for post in posts:
        if post.scheduled is not None:
            if post.scheduled <= future and post.scheduled >= now:
                # TODO: post on Twitter
                next_date = post.scheduled + timedelta(post.days)
                post.scheduled = post.scheduled + timedelta(post.days)
                db.session.commit()

if __name__ == '__main__':
    with app.app_context():
        print current_user
    publisher()